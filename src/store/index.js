import {createStore, applyMiddleware, combineReducers} from 'redux';
import logger from 'redux-logger';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {persistStore, persistReducer} from 'redux-persist';
import Reducers from '../reducers';
import thunk from 'redux-thunk';

const Reducer = {
  appData: Reducers,
};
const allReducer = combineReducers(Reducer);

export const Store = createStore(allReducer, applyMiddleware(thunk, logger));
